package conway2;

import java.io.IOException;
import java.nio.charset.Charset;

import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

public class MyTerminal {
	private Terminal terminal;
	private int currentLine=0;
	
	public MyTerminal() throws IOException {
	   	this.terminal=new DefaultTerminalFactory(System.out, System.in, Charset.forName("UTF8")).createTerminal();
	   	terminal.enterPrivateMode();
	   	clearScreen();
	}
	
	void clearScreen() throws IOException {
		terminal.clearScreen();
		currentLine=0;
		terminal.setCursorPosition(0, 0);
	}
	
	void println(final String line) throws IOException {
    	for(char c : line.toCharArray())
    		terminal.putCharacter(c);
    	currentLine++;
    	terminal.setCursorPosition(0, currentLine);
    	terminal.flush();
	}
	
	void exit() throws IOException {
		terminal.exitPrivateMode();
	}

}
