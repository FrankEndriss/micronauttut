package conway2;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.happypeople.conway1.machine.Cell;
import com.happypeople.conway1.machine.StandardUniverse;
import com.happypeople.conway1.machine.Universe;

import io.micronaut.configuration.picocli.PicocliRunner;
import io.micronaut.context.ApplicationContext;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "conway", description = "...",
        mixinStandardHelpOptions = true)
public class Conway2Command implements Runnable {
    private static Logger log = LoggerFactory.getLogger(Conway2Command.class);

    @Option(names = {"-v", "--verbose"}, description = "...")
    boolean verbose;

	private MyTerminal terminal;

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(Conway2Command.class, args);
    }

    private void dump(final Collection<Cell> cells) throws IOException
    {
    	terminal.clearScreen();

        int minX = Integer.MAX_VALUE;
        int maxX = Integer.MIN_VALUE;
        int minY = Integer.MAX_VALUE;
        int maxY = Integer.MIN_VALUE;

        for (final Cell cell : cells)
        {
            minX = Math.min(minX, cell.getX());
            maxX = Math.max(maxX, cell.getX());
            minY = Math.min(minY, cell.getY());
            maxY = Math.max(maxY, cell.getY());
        }
        log.info("dumping; minX=" + minX + " maxX=" + maxX + " minY=" + minY + " maxY=" + maxY);
        log.info("" + cells);

        final StringBuilder neutralLine = new StringBuilder();
        for (int i = minX; i <= maxX; i++)
        {
            neutralLine.append("-");
        }
        final StringBuilder[] lines = new StringBuilder[maxY - minY + 1];
        for (int i = 0; i <= maxY - minY; i++)
        {
            lines[i] = new StringBuilder(neutralLine.toString());
        }
        for (final Cell cell : cells)
        {
            lines[cell.getY() - minY].setCharAt(cell.getX() - minX, 'X');
        }

        for (final StringBuilder sb : lines)
        {
        	terminal.println(sb.toString());
            // log.info(sb.toString());
        }
        log.info("");;
        try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    private void runConway() throws IOException {
        if(verbose)
            System.out.println("Hi!");

        log.info("running main...");
        final Universe universe = new StandardUniverse();
        final List<Cell> initCells = new ArrayList<Cell>();
        initCells.add(new Cell(3, 3));
        initCells.add(new Cell(3, 4));
        initCells.add(new Cell(3, 5));
        // initCells.add(new Cell(3, 6));
        // initCells.add(new Cell(4, 2));
        initCells.add(new Cell(4, 4));
        initCells.add(new Cell(4, 5));
        initCells.add(new Cell(5, 2));
        initCells.add(new Cell(5, 3));
        initCells.add(new Cell(6, 2));
        initCells.add(new Cell(6, 4));
        initCells.add(new Cell(7, 4));
        initCells.add(new Cell(8, 4));
        initCells.add(new Cell(9, 4));
        initCells.add(new Cell(10, 5));
        universe.setLivingCells(initCells);

        int count = 0;
        int changes = 0;
        Collection<Cell> cells;
        do
        {
            count++;
            cells = universe.getLivingCells();
            dump(cells);
            changes = universe.tic();
        }
        while (cells.size() > 0 && changes > 0);
        log.info("finished main.");
	
    }

    public void run()
    {
    	try {
    		terminal=new MyTerminal();
			runConway();
			terminal.exit();
		} catch (final Exception e) {
			log.error(e.getMessage(), e);
		}
    }
}
