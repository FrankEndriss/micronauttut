package com.happypeople.conway1.machine;

import java.util.Collection;

public interface Universe
{
    /**
     * Used to initiate a Universe.
     *
     * @param initCells
     */
    void setLivingCells(Collection<Cell> initCells);

    /**
     * Used to step one tic further.
     * 
     * @return number of changed cells (new or died)
     */
    int tic();

    /** Used to read the current state of the universe */
    Collection<Cell> getLivingCells();

}
