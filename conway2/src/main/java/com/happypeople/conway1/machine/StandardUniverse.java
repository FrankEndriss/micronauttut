package com.happypeople.conway1.machine;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StandardUniverse implements Universe
{
    private Set<Cell> cells = new HashSet<Cell>();

    @Override
    public void setLivingCells(final Collection<Cell> initCells)
    {
        cells.clear();
        cells.addAll(initCells);
    }

    private boolean checkUmliegendeCount(final Cell cell, final Collection<Cell> umliegende, final int minCount, final int maxCount)
    {
        int count = 0;

        final Cell LO = cell.getLO();
        if (umliegende != null)
        {
            umliegende.add(LO);
        }
        if (cells.contains(LO))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell O = cell.getO();
        if (umliegende != null)
        {
            umliegende.add(O);
        }
        if (cells.contains(O))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell RO = cell.getRO();
        if (umliegende != null)
        {
            umliegende.add(RO);
        }
        if (cells.contains(RO))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell L = cell.getL();
        if (umliegende != null)
        {
            umliegende.add(L);
        }
        if (cells.contains(L))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell R = cell.getR();
        if (umliegende != null)
        {
            umliegende.add(L);
        }
        if (cells.contains(R))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell LU = cell.getLU();
        if (umliegende != null)
        {
            umliegende.add(LU);
        }
        if (cells.contains(LU))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell U = cell.getU();
        if (umliegende != null)
        {
            umliegende.add(U);
        }
        if (cells.contains(U))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        final Cell RU = cell.getRU();
        if (umliegende != null)
        {
            umliegende.add(RU);
        }
        if (cells.contains(RU))
        {
            count++;
            if (count > maxCount)
            {
                return false;
            }
        }

        return count >= minCount;
    }

    @Override
    public int tic()
    {
        // standard rules: B3/S23

        // Zwei-Schritt-verfahren:
        // 1. Prüfen aller living ob die, dabei sammeln der umliegenden.
        final Set<Cell> umliegende = new HashSet<Cell>();
        final int countBefore = cells.size();
        final Set<Cell> stillLiving = cells.stream().filter((cell) -> checkUmliegendeCount(cell, umliegende, 2, 3)).collect(Collectors.toCollection(() -> new HashSet<Cell>()));
        final int died = countBefore - stillLiving.size();

        // 2. Prüfen aller umliegenden, ob begin.
        final List<Cell> newLiving = umliegende.stream().filter((cell) -> !cells.contains(cell)).filter((cell) -> checkUmliegendeCount(cell, null, 3, 3))
                .collect(Collectors.toList());
        stillLiving.addAll(newLiving);
        cells = stillLiving;

        return newLiving.size() + died;
    }

    @Override
    public Collection<Cell> getLivingCells()
    {
        // return Collections.unmodifiableCollection(cells);
        return cells;
    }

}
