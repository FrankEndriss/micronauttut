package com.happypeople.conway1.machine;

public class Cell
{
    private final int x;
    private final int y;

    public Cell(final int x, final int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * X coordinate of this cell.
     *
     * @return
     */
    public int getX()
    {
        return x;
    }

    /**
     * Y coordinate of this cell.
     *
     * @return
     */
    public int getY()
    {
        return y;
    }

    public Cell getLO()
    {
        return new Cell(x - 1, y - 1);
    }

    public Cell getO()
    {
        return new Cell(x, y - 1);
    }

    public Cell getRO()
    {
        return new Cell(x + 1, y - 1);
    }

    public Cell getL()
    {
        return new Cell(x - 1, y);
    }

    public Cell getR()
    {
        return new Cell(x + 1, y);
    }

    public Cell getLU()
    {
        return new Cell(x - 1, y + 1);
    }

    public Cell getU()
    {
        return new Cell(x, y + 1);
    }

    public Cell getRU()
    {
        return new Cell(x + 1, y + 1);
    }

    // generated *****************************************************

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(final Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Cell other = (Cell) obj;
        if (x != other.x)
        {
            return false;
        }
        if (y != other.y)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Cell [x=" + x + ", y=" + y + "]";
    };

}
