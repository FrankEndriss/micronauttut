package euler164;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.configuration.picocli.PicocliRunner;
import picocli.CommandLine.Command;

@Command(name = "euler164", description = "...", mixinStandardHelpOptions = true)
public class Euler164Command implements Runnable {
	private static Logger log = LoggerFactory.getLogger(Euler164Command.class);

	public static void main(String[] args) throws Exception {
		PicocliRunner.run(Euler164Command.class, args);
	}

	public void run() {
		log.info("result: " + calcPossibilities(0, 0, 20));
	}

	private Map<String, BigInteger> results=new HashMap<String, BigInteger>();

	/**
	 * Berechne die Anzahl von Möglichkeiten für zwei gegebene Ziffern und eine
	 * gegebene Restlänge der Zahl.
	 */
	private BigInteger calcPossibilities(final int p1, final int p2, final int digits) {
		
		if(digits == 1)
			return BigInteger.valueOf(9-p1-p2);
		
		final String key=""+p1+p2+digits;
		if(results.containsKey(key))
			return results.get(key);

		final int maxNextDigit = 9 - p1 - p2;
		BigInteger sum = BigInteger.ZERO;
		for (int i = 0; i <= maxNextDigit; i++) {
			sum = sum.add(calcPossibilities(p2, i, digits - 1));
		}

		results.put(key, sum);
		if(p1==0 && p2==0)
			log.info("p1="+p1+" p2="+p2+" level=" + digits+" sum="+sum);
		return sum;
	}
}
