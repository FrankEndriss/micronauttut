package euler637;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.configuration.picocli.PicocliRunner;

import picocli.CommandLine.Command;

@Command(name = "euler637", description = "...",
        mixinStandardHelpOptions = true)
public class Euler637Command implements Runnable {
	private static Logger log = LoggerFactory.getLogger(Euler637Command.class);

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(Euler637Command.class, args);
    }

    public void run() {
    	g(100, 10, 3);
    	// g(10000000, 10, 3);
    }
    
    void g(final int number, final int base1, final int base2) {
    	int sum=0;
    	for(int i=0; i<=number; i++) {
    		if(f(i, base1)==f(i, base2))
    			sum+=i;
    	}
    	log.info("g("+number+", "+base1+", "+base2+")="+sum);
    }
    
    int f(int number, final int base) {
    	int steps=0;
    	while(number>=base) {
    		number=minNumberOfSteps(number, base);
    		steps++;
    	}
    	return steps;
    }
    
    /** Berechnet die Minimale Anzahl von Transoformationen bei Darstellung von number zur Basis base
     * um eine einstellige Darstellung zu erhalten.
     * @param number Die Zahl
     * @param base Die Darstellungs-Basis
     * @return Die Quersumme
     */
    int minNumberOfSteps(final int number, final int radix) {
    	if(radix>Character.MAX_RADIX)
    		throw new RuntimeException("sorry, MAX_RADIX="+Character.MAX_RADIX);

    	String numberStr=BigInteger.valueOf(number).toString(radix);
    	if(numberStr.length()==1)
    		return 0;

    	int minSteps=Integer.MAX_VALUE;
    	int possibleTransforms=numberOfTransformations(number, radix);
    	for(int t=1; t<possibleTransforms; t++) {
    		int transformed=transform(number, radix, t);
    		int steps=minNumberOfSteps(transformed, radix);
    		if(steps<minSteps)
    			minSteps=steps;
    	}
    	return minSteps;
    }
    
    /** Führt die übergebene Transformation aus und returnt das Ergebnis.
     * @param number
     * @param radix
     * @param transform
     * @return
     */
    int transform(final int number, final int radix, final int transform) {
    	final String transfStr=BigInteger.valueOf(transform).toString(2);
    	String numberStr=BigInteger.valueOf(number).toString(radix);
    	log.info("transform(), number="+numberStr+" transform="+transfStr);
    	int pos=1;
    	int sum=0;
    	for(char c : transfStr.toCharArray()) {
    		if(c=='1') {
    			pos=1;
    			final String cuttedString=numberStr.substring(0, pos);
    			numberStr=numberStr.substring(pos);
    			sum+=new BigInteger(cuttedString, radix).intValue();
    		} else
    			pos++;
    	}
    	return sum;
    }
    
    /** Calculates the number of possible transformations. Which is the
     * number of possible permutations of substrings of a given number string.
     * @param number 
     * @param radix
     * @return The number of possible transformations given the defintion: Given any positive integer n, we
     * can construct a new integer by inserting plus
     * signs between some of the digits of the base B representation of n, and then carrying out the additions.
     */
    int numberOfTransformations(final int number, final int radix) {
    	final String s=BigInteger.valueOf(number).toString(radix);
    	return BigInteger.valueOf(2).pow(s.length()-1).intValue()-1;
    }
}
